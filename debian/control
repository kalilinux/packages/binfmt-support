Source: binfmt-support
Section: admin
Priority: optional
Maintainer: Colin Watson <cjwatson@debian.org>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9~), pkg-config, libpipeline-dev, dh-autoreconf, dh-systemd
Homepage: http://binfmt-support.nongnu.org/
Vcs-Git: https://anonscm.debian.org/git/binfmt-support/binfmt-support.git
Vcs-Browser: https://anonscm.debian.org/cgit/binfmt-support/binfmt-support.git

Package: binfmt-support
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, lsb-base (>= 4.1+Debian3)
Multi-Arch: foreign
Description: Support for extra binary formats
 The binfmt_misc kernel module, contained in versions 2.1.43 and later of the
 Linux kernel, allows system administrators to register interpreters for
 various binary formats based on a magic number or their file extension, and
 cause the appropriate interpreter to be invoked whenever a matching file is
 executed. Think of it as a more flexible version of the #! executable
 interpreter mechanism.
 .
 This package provides an 'update-binfmts' script with which package
 maintainers can register interpreters to be used with this module without
 having to worry about writing their own init.d scripts, and which sysadmins
 can use for a slightly higher-level interface to this module.
